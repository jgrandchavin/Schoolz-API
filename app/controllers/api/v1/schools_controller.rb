class Api::V1::SchoolsController < Api::ApiController

  swagger_controller :schools, "School Management"

  # index()
  #
  # Retrieve all school (can filter by status: 'private' or 'public')
  #
  # @return Json
  #
  swagger_api :index do
    summary "Show all schools"
    param :header, "AUTHORIZATION", :string, :required, "Token to authenticate"
    response :success
    response 422, "Unsuccessful"
    response :unauthorized
  end
  def index
    @schools = School.filter(params)
  end

  # show()
  #
  # Retrieve a school
  #
  # @return Json
  #
  swagger_api :show do
    summary "Show a school"
    param :header, "AUTHORIZATION", :string, :required, "Token to authenticate"
    param :path, "id", :string,  :required, "Id of the school to show"
    response :success
    response 422, "Unsuccessful"
    response :unauthorized
  end
  def show
    @school = School.find(params[:id])

    if @school.errors.any?
      render json: { success: false, errors: @school.errors.messages }, status: 422
    else
      render template: 'api/v1/schools/show', status: 201
    end
  end

  # create()
  #
  # Create new school from school_params
  #
  # @return 401 if no error
  # @return 422 if there is an error
  #
  swagger_api :create do
    summary "Create a new school"
    param :header, "AUTHORIZATION", :string, :required, "Token to authenticate"
    param :body, "School in JSON", :json,  :required, "Example: { \"name\": \"test\"}"
    response :success
    response 422, "Unsuccessful"
    response :unauthorized
  end
  def create
    @school = School.create(school_params)

    if @school.errors.any?
      render json: { success: false, errors: @school.errors.messages }, status: 422
    else
      render template: 'api/v1/schools/show', status: 201
    end
  end

  # update()
  #
  # Update a school with ID param & school in body
  #
  # @return 201 if no error
  # @return 422 if there is an error
  #
  swagger_api :update do
    summary "Update a school"
    param :header, "AUTHORIZATION", :string, :required, "Token to authenticate"
    param :path, "id", :string,  :required, "Id of the school to update"
    param :body, "School in JSON", :json,  :required, "school: { \"name\": \"test\"}"
    response :success
    response 422, "Unsuccessful"
    response :unauthorized
  end
  def update
    @school = School.update(params[:id], school_params)
    if @school.errors.any?
      render json: { success: false, errors: @school.errors.messages }, status: 422
    else
      render template: 'api/v1/schools/show', status: 201
    end
  end

  # destroy()
  #
  # Destroy a school with ID param
  #
  # @return 201 if no error
  # @return 422 if there is an error
  #
  swagger_api :destroy do
    summary "Delete a school"
    param :header, "AUTHORIZATION", :string, :required, "Token to authenticate"
    param :path, "id", :string,  :required, "Id of the school to delete"
    response :success
    response 422, "Unsuccessful"
    response :unauthorized
  end
  def destroy
    @school = School.destroy(params[:id])

    if @school.errors.any?
      render json: { success: false, errors: @school.errors.messages }, status: 422
    else
      render json: { success: true, message: 'Delete Success' }, status: 201
    end
  end

  private

  # school_params() : private
  #
  # Let pass only permit params
  #
  def school_params
    params.require(:school).permit(
      :name,
      :address,
      :city,
      :zip_code,
      :latitude,
      :longitude,
      :status,
      :students_count,
      :opening_hours,
      :email,
      :phone
    )
  end
end
