Swagger::Docs::Config.register_apis({
    "1.0" => {
        :api_file_path => "public",
        :base_path => "https://schoolz-api.herokuapp.com",
        :clean_directory => true,
        :parent_controller => Api::ApiController,
        :attributes => {
            :info => {
                "title" => "Schoolz API",
                "description" => "Manage school for an Android App",
                "termsOfServiceUrl" => "",
                "contact" => "julien.grandchavin@icloud.com",
                "license" => "Apache 2.0",
                "licenseUrl" => "http://www.apache.org/licenses/LICENSE-2.0.html"
            }
        }
    }
})