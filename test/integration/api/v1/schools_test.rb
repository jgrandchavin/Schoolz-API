require 'test_helper'

feature 'Schools' do

  # INDEX METHOD TEST
  #
  describe '#index' do

    it 'returns 401 when there is no token in header' do
      get api_v1_schools_path

      assert_equal 401, last_response.status
    end

    it 'returns 401 when there is a bad token in header' do
      get api_v1_schools_path, nil, {'HTTP_AUTHORIZATION' => '34567890'}

      assert_equal 401, last_response.status
    end

    it 'returns 200 when there is a good token in header' do
      get api_v1_schools_path, nil, {'HTTP_AUTHORIZATION' => 'valid_token'}

      assert_equal 200, last_response.status
    end

    it 'returns a list of all schools' do
      get api_v1_schools_path, nil, {'HTTP_AUTHORIZATION' => 'valid_token'}

      assert_equal 2, json_response['schools'].length
    end

    it 'return only private schools' do
      get api_v1_schools_path, {status: 'private'}, {'HTTP_AUTHORIZATION' => 'valid_token'}

      assert_equal 1, json_response['schools'].length
      assert_equal 'private', json_response['schools'].first['status']
    end

    it 'return only public schools' do
      get api_v1_schools_path, {status: 'public'}, {'HTTP_AUTHORIZATION' => 'valid_token'}

      assert_equal 1, json_response['schools'].length
      assert_equal 'public', json_response['schools'].first['status']
    end

  end

  # CREATE METHOD TEST
  #
  describe "#create" do

    it 'return 201 when school successfully created' do

      assert_difference "School.all.count" do
        post api_v1_schools_path, {
            'school': {
                'name': 'new school',
                'longitude': 1.0,
                'latitude': 1.0,
                'email': 'name@domain.com'
            }
        }, {'HTTP_AUTHORIZATION' => 'valid_token'}

        assert_equal 201, last_response.status
        assert_equal 'new school', json_response['school']['name']
      end
    end

    it 'return 422 when school creation failed because of bad email' do

      assert_no_difference "School.all.count" do
        post api_v1_schools_path, {
            'school': {
                'name': 'new school',
                'longitude': 1.0,
                'latitude': 1.0,
                'email': 'namedomain.com'
            }
        }, {'HTTP_AUTHORIZATION' => 'valid_token'}

        assert_equal 422, last_response.status
      end
    end

    it 'return 422 when school creation failed because of params is missing ' do

      assert_no_difference "School.all.count" do
        post api_v1_schools_path, {
            'school': {
                'name': 'test'
            }
        }, {'HTTP_AUTHORIZATION' => 'valid_token'}

        assert_equal 422, last_response.status
      end
    end
  end

  # SHOW METHOD TEST
  #
  describe "#show" do
    it 'return 201 when school successfully showed' do

        get api_v1_school_path(0), nil, {'HTTP_AUTHORIZATION' => 'valid_token'}

        assert_equal 201, last_response.status
        assert_equal 'Ynov Lyon', json_response['school']['name']
    end
  end

  # DESTROY METHOD TEST
  #
  describe "#destroy" do
    it 'return 201 when school successfully deleted' do

      assert_difference "School.all.count", -1 do
        delete api_v1_school_path(0), nil, {'HTTP_AUTHORIZATION' => 'valid_token'}

        assert_equal 201, last_response.status
      end
    end
  end

  # UPDATE METHOD TEST
  #
  # TODO : FAILED BUT UPDATE WORKS WITH POSTMAN... DON'T UNDERSTAND WHY...
  describe "#update" do
    it "return 201 when school successfully updated" do
      put api_v1_school_path(0), {"school": {"name": "updated"}}, {"HTTP_AUTHORIZATION" => 'valid_token'}

      assert_equal 201, last_response.status
      assert_equal 'updated', json_response['school']['name']
    end
  end

end